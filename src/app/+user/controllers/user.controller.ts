import { Controller, Get } from '@nestjs/common';

import { UserDbService } from '../services/user-db.service';
import { UserEntity } from '../entities';

@Controller('users')
export class UserController {


  public constructor(
    private readonly $userDb: UserDbService,
  ) {}

  @Get()
  public getUsers(): Promise<UserEntity[]> {
    return this.$userDb.findAll();
  }
}
