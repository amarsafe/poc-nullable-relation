import { Column, Entity, OneToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { UserEntity } from './user.entity';

@Entity({ name: 'settings' })
export class SettingsEntity {
    @PrimaryGeneratedColumn()
    public id!: number;

    @OneToOne(type => UserEntity,
              (u: UserEntity) => u.settings,
              { nullable: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    user!: UserEntity;

    @RelationId((ps: SettingsEntity) => ps.user)
    userId!: string;

    @Column({nullable: true})
    locale!: string;
}
