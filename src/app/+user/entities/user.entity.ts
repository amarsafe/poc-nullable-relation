import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, RelationId} from 'typeorm';
import {SettingsEntity} from "./settings.entity";

/**
 * This is a demo entity. Feel free to remove/update it.
 */
@Entity()
export class UserEntity {

  @PrimaryGeneratedColumn()
  public id!: number;

  @Column()
  public name!: string;

  @OneToOne(type => SettingsEntity,
      settings => settings.user,
      {nullable: true})
  @JoinColumn()
  settings!: SettingsEntity;

  @RelationId((userAuth: UserEntity) => userAuth.settings)
  settingsId!: string;

}
