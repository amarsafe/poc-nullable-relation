import { Type } from '@nestjs/common';

import { UserEntity } from './user.entity';
import { SettingsEntity } from './settings.entity';

export * from './user.entity';
export * from './settings.entity';

export const entities: Type<any>[] = [
  UserEntity,
  SettingsEntity,
];
