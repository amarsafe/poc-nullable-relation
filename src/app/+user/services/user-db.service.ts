import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { UserEntity } from '../entities';

/**
 * This is a demo db service. Feel free to remove/update it.
 */
@Injectable()
export class UserDbService {

  public constructor(
    @InjectRepository(UserEntity)
    private readonly $userRepo: Repository<UserEntity>,
  ) {}

  public findAll(): Promise<UserEntity[]> {
    return this.$userRepo.find();
  }

}
