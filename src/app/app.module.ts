import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';


import { entities as userEntities } from './+user/entities';
import { UserModule } from './+user/user.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import {getDefaultConnection} from "../configs/ormconfig";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...getDefaultConnection(),
      entities: [...userEntities],
    }),
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
