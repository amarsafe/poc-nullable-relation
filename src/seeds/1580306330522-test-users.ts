import { Like } from 'typeorm';
import * as _ from 'lodash';

import { BaseSeed } from '../base-seed';
import {SettingsEntity, UserEntity} from "../app/+user/entities";

const fakeUser = (name: string) => _.assign(new UserEntity(), { name });

/**
 * This is a demo seed. Feel free to remove/update it.
 */
export class TestUsers1580306330522 extends BaseSeed {
  public name = 'TestUsers1580306330522';

  public async up(): Promise<any> {
    const repo = this.getRepository(UserEntity);
    const repoSetting = this.getRepository(SettingsEntity);

    const settings = await repoSetting.insert({});
    await repo.insert([
      {
        ...fakeUser('Ivan (TypeORM Test)'),
        settings: {
          id: settings.raw[0].id,
        },
      },
      fakeUser('Kate (TypeORM Test)'),
    ]);
  }

  public async down(): Promise<any> {
    const repo = this.getRepository(UserEntity);

    await repo.delete({ name: Like('%(TypeORM Test)') });
  }

}
